import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  deleteItem(key: string) {
    this.authService.user.subscribe(user =>{
        this.db.list('/users/'+user.uid+'/items').remove(key);
      })
  }
 
  addTodo(text1: string,text2:string) {
    this.authService.user.subscribe(
      user => {
        this.db.list('/users/'+user.uid+'/items').push({'nameOfBook': text1,'nameOfAuthor': text2});
      }
    )
  }
 
  update(key,text1,text2){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/items').update(key,{'nameOfBook': text1,'nameOfAuthor': text2})
    })
  }
  
updateDone(key:string, text:string, stock:boolean)
{
  this.authService.user.subscribe(user =>{
    this.db.list('/users/'+user.uid+'/items').update(key,{'stock':stock});
 })

}

  constructor(private authService:AuthService,
    private db:AngularFireDatabase,
  ) { }

}
