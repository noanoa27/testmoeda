import { Component, OnInit,Input } from '@angular/core';
import { ItemsService } from '../items.service';

@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})

export class ItemComponent implements OnInit {
 
  @Input() data:any;
  text1;
  text2;
  key;
  tempText1;
  tempText2;
checkboxFlag: boolean;
  showTheButton=true; //מחיקה
  showDeleteField=false;//כפתורים נוספים
  showEditField=false;
  showTheText=false;
  
checkChange()  {
  this.ItemsService.updateDone(this.key,this.text1,this.checkboxFlag);
}
  showButton(){ //עריכה
    if(!this.showEditField){ 
      this.showTheText=true;
      //this.showTheButton=true;
    }
  }
  hideButton(){
    this.showTheText=false;
  }
  showDelete(){ 
    this.showDeleteField=true;
    this.showTheButton=false;
  }
  cancel(){
    this.showDeleteField=false;
    this.showTheButton=true;
  } 
  showEdit(){ //עריכה
    this.showEditField=true;
    this.showTheButton=false;
    this.tempText1=this.text1;
    this.tempText2=this.text2;
  }
  save(){ //שמירת עריכה
    this.ItemsService.update(this.key,this.text1,this.text2)
    this.showEditField=false;
  }
  cancelEdit(){ //לבטל עריכה
    this.showEditField=false;
    this.text1=this.tempText1;
    this.text2=this.tempText2;
  }
delete() {
  this.ItemsService.deleteItem(this.key);
}

  constructor(private ItemsService:ItemsService) { }

  ngOnInit() {
    this.text1 = this.data.nameOfBook;
    this.text2= this.data.nameOfAuthor;
    this.key = this.data.$key;
    this.checkboxFlag = this.data.stock;
  }

}
