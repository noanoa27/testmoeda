import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database'
import {AuthService} from '../auth.service';
import {ItemsService} from '../items.service';

@Component({
  selector: 'books',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  items=[];
  text1:string;
  text2:string;
  user=[];
  nickname;

  addTodo() {
    this.ItemsService.addTodo(this.text1,this.text2);
    this.text1 = '';
    this.text2 = '';
  }
 
  constructor(private db:AngularFireDatabase, 
    private authService:AuthService,
  private ItemsService:ItemsService) { }

  ngOnInit() {
    this.authService.user.subscribe(
      user => {
        if(!user) return;

        this.db.list('/users/'+user.uid+'/details').snapshotChanges().subscribe(
          details => {
            details.forEach(
                  detail => {
                    let y = detail.payload.toJSON();
                    this.user.push(y);            
                     this.nickname = this.user[0].nickname;         
              }
            )
            
           
          }
        )
    this.db.list('/users/'+user.uid+'/items').snapshotChanges().subscribe(
      items => {
        this.items = [];
        items.forEach(
            item=>{
              let y =item.payload.toJSON();
              y["$key"] = item.key;
              this.items.push(y);
            }
          )
 
      }
    )
 
 
  }
    )
  }  }
